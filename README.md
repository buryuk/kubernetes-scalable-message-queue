# Kubernetes Scalable Message Queue

## Environment

Ubuntu 20.04

## Steps

1. Install and Set Up kubectl on Linux
- Download kubectl v.1.21.5 because DigitalOcean uses this version of Kubernetes:
    ```sh
    curl -LO https://dl.k8s.io/release/v1.21.5/bin/linux/amd64/kubectl
    ```

- Install kubectl:
    ```sh
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    ```
    
- Test to ensure the version you installed:
    ```sh
    kubectl version --client
    ```

Details: [Install and Set Up kubectl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) - Install kubectl binary with curl on Linux

2. Create DigitalOcean Kubernetes Cluster

- From the Create menu in the control panel, click Kubernetes.
- Select a Kubernetes version. The latest version is selected by default and is the best choice if you have no specific need for an earlier version.
- Choose a datacenter region.
- Customize the default node pool, choose the node pool names, and add additional node pools.
- Name the cluster, select the project you want the cluster to belong to, and optionally add a tag. Any tags you choose will be applied to the cluster and its worker nodes.
- Click Create Cluster. Provisioning the cluster takes several minutes.
- Download the cluster configuration file by clicking Actions, then Download Config from the cluster home page.

Note: You need to have at least 2 Droplet limit. DigitalOcean cloud accounts have 1 Droplet limit initially. You need to Request Increase via control panel. 

Details: [Kubernetes Quickstart](https://docs.digitalocean.com/products/kubernetes/quickstart/) - Create Clusters
[How to Connect to a DigitalOcean Kubernetes Cluster](https://docs.digitalocean.com/products/kubernetes/how-to/connect-to-cluster/) - Download from the Control Panel

3. Download Strimzi and install it.

- Download the strimzi-0.26.0.zip file from GitHub.
- Unzip the file to any destination:
    ```sh
    unzip strimzi-xyz.zip
    ```
- Create a new kafka namespace for the Strimzi Kafka Cluster Operator.
    ```sh
    kubectl create ns kafka
    ```
- Modify the installation files to reference the kafka namespace where you will install the Strimzi Kafka Cluster Operator.
    ```sh
    sed -i 's/namespace: .*/namespace: kafka/' install/cluster-operator/*RoleBinding*.yaml
    ```
- Create a new my-kafka-project namespace where you will deploy your Kafka cluster
    ```sh
    kubectl create ns my-kafka-project
    ```
- Edit the install/cluster-operator/060-Deployment-strimzi-cluster-operator.yaml file and set the STRIMZI_NAMESPACE environment variable to the namespace my-kafka-project:
    ```sh
    ...
    env:
    - name: STRIMZI_NAMESPACE
    value: my-kafka-project
    ...
    ```
- Deploy the CRDs and role-based access control (RBAC) resources to manage the CRDs.
    ```sh
    kubectl create -f install/cluster-operator/ -n kafka
    ```
- Give permission to the Cluster Operator to watch the my-kafka-project namespace.
    ```sh
    kubectl create -f install/cluster-operator/020-RoleBinding-strimzi-cluster-operator.yaml -n my-kafka-project
    ```
    ```sh
    kubectl create -f install/cluster-operator/031-RoleBinding-strimzi-cluster-operator-entity-operator-delegation.yaml -n my-kafka-project
    ```
4. Creating a cluster
- Create a new my-cluster Kafka cluster with one ZooKeeper and one Kafka broker
    Expose the Kafka cluster outside of the Kubernetes cluster using an external listener configured to use a nodeport: 
    ```sh
    cat << EOF | kubectl create -n my-kafka-project -f -
    apiVersion: kafka.strimzi.io/v1beta2
    kind: Kafka
    metadata:
    name: my-cluster
    spec:
    kafka:
        replicas: 1
        listeners:
        - name: plain
            port: 9092
            type: internal
            tls: false
          - name: tls
            port: 9093
            type: internal
            tls: true
            authentication:
              type: tls
          - name: external
            port: 9094
            type: nodeport
            tls: false
        storage:
        type: jbod
        volumes:
        - id: 0
            type: persistent-claim
            size: 100Gi
            deleteClaim: false
      config:
          offsets.topic.replication.factor: 1
         transaction.state.log.replication.factor: 1
         transaction.state.log.min.isr: 1
     zookeeper:
       replicas: 1
        storage:
         type: persistent-claim
         size: 100Gi
         deleteClaim: false
     entityOperator:
       topicOperator: {}
       userOperator: {}
    EOF
    ```
- Wait for the cluster to be deployed:
    ```sh
    kubectl wait kafka/my-cluster --for=condition=Ready --timeout=300s -n my-kafka-project
    ```
- When your cluster is ready, create a topic to publish and subscribe from your external client.

    ```sh
    cat << EOF | kubectl create -n my-kafka-project -f -
    apiVersion: kafka.strimzi.io/v1beta2
    kind: KafkaTopic
    metadata:
        name: my-topic
        labels:
            strimzi.io/cluster: "my-cluster"
    spec:
        partitions: 3
        replicas: 1
    EOF
    ```
    [Strimzi Quick Start guide (0.26.0)](https://strimzi.io/docs/operators/latest/quickstart.html)

5. Sending and receiving messages from a topic
- Create a Kafka producer pod: 
    ```sh
    kubectl run kafka-producer -ti -n my-kafka-project --image=strimzi/kafka:latest-kafka-2.4.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list my-cluster-kafka-bootstrap:9092 --topic my-topic
    ```
- Write some text. 
- Exit with Ctrl+C
- Create a Kafka consumer pod: 
    ```sh
    kubectl run kafka-consumer -ti -n my-kafka-project --image=strimzi/kafka:latest-kafka-2.4.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server my-cluster-kafka-bootstrap:9092 --topic my-topic --from-beginning
    ```
- See the text you wrote via Kafka producer pod. 
Please see screenshot below: 

    ![This is an image](app-screenshot.PNG)

## License
MIT License

Copyright (c) 2021 Burak Yuksel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
